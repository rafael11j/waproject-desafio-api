export interface IRequest {
  id?: number;
  description?: string;
  amount?: number;
  price?: number;
  createdDate?: Date;
  updatedDate?: Date;
}
