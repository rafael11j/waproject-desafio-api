import { Injectable } from '@nestjs/common';
import { IRequest } from 'modules/database/interfaces/request';
import { Request } from 'modules/database/models/request';
import { Transaction } from 'objection';

@Injectable()
export class RequestRepository {
  public async findAll(transaction: Transaction = null): Promise<Request[]> {
    return Request.query(transaction);
  }

  public async findById(id: number, transaction: Transaction = null): Promise<Request> {
    return Request.query(transaction).findById(id);
  }

  public async insert(model: IRequest, transaction: Transaction = null): Promise<Request> {
    return Request.query(transaction).insertAndFetch(model as any);
  }

  public async update(model: IRequest, transaction: Transaction = null): Promise<Request> {
    return Request.query(transaction).updateAndFetchById(model.id, model as any);
  }

  public async delete(requestId: string, transaction: Transaction = null): Promise<void> {
    await Request.query(transaction).deleteById(requestId);
  }
}
