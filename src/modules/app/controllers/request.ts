import { Body, Controller, Get, NotFoundException, Post, Put, Param, Delete } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request } from 'modules/database/models/request';

import { RequestRepository } from '../repositories/request';
import { RequestValidator } from '../validators/request';

@ApiTags('App: Request')
@Controller('/request')
export class RequestController {
  constructor(private requestRepository: RequestRepository) {}

  @Get()
  @ApiResponse({ status: 200, type: Request })
  public async findAll() {
    const requests = this.requestRepository.findAll();

    return requests;
  }

  @Get('/:id')
  @ApiResponse({ status: 200, type: Request })
  public async find(@Param() params: any) {
    const request = await this.requestRepository.findById(params.id);
    if (request == null) throw new NotFoundException();

    return request;
  }

  @Post()
  @ApiResponse({ status: 200, type: Request })
  public async store(@Body() model: RequestValidator) {
    return this.requestRepository.insert(model);
  }

  @Put()
  @ApiResponse({ status: 200, type: Request })
  public async update(@Body() model: RequestValidator) {
    return this.requestRepository.update(model);
  }

  @Delete('/:id')
  @ApiResponse({ status: 200, type: Request })
  public async delete(@Param() params: any) {
    const request = await this.requestRepository.findById(params.id);
    if (request == null) throw new NotFoundException();

    this.requestRepository.delete(params.id);
  }
}
