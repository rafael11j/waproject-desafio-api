import { Body, Controller, Get, NotFoundException, Post } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthRequired, CurrentUser } from 'modules/common/guards/token';
import { ICurrentUser } from 'modules/common/interfaces/currentUser';
import { User } from 'modules/database/models/user';

import { UserRepository } from '../repositories/user';
import { UserService } from '../services/user';
import { UpdateValidator } from '../validators/profile/update';

@ApiTags('App: Teste')
@Controller('/teste')
export class TesteController {
  constructor(private userRepository: UserRepository, private userService: UserService) {}

  @Get()
  @ApiResponse({ status: 200, type: User })
  public async details(@CurrentUser() currentUser: ICurrentUser) {
    const user = { name: 'Rafael', idade: 22 };
    if (!user) throw new NotFoundException();

    return user;
  }

  @Post()
  @ApiResponse({ status: 200, type: User })
  public async update(@Body() model: UpdateValidator, @CurrentUser() currentUser: ICurrentUser) {
    return this.userService.update(model, currentUser);
  }
}
