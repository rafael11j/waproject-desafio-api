import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class RequestValidator {
  @IsNotEmpty()
  @ApiProperty({ required: true })
  public description: string;

  @IsNotEmpty()
  @ApiProperty({ required: true })
  public amount: number;

  @IsNotEmpty()
  @ApiProperty({ required: true })
  public price: number;
}
